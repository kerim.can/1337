-Ide: Integierte Entwicklungsumgebung
-Cd: Verzeichnise zu Wechseln
-ls: Inhalt des Verzeichnis anzeigen
-ssh: Secure Shell: auf einen anderen Computer zugreifen
-SSD: Solid State Drive: Festplatte
-HDD: Hard Disk Drive: Festplatte
-www: World Wide Web
-HTTPS: Hypertect Transfer Protokoll Secure 
-Vi: visual
-Git: Versionsvewaltung
-shell: äüßere Schicht des Betriebssytems.
-/home = lässt sie in den Home-Verzeichnis wechseln 
-/Boot = Statische Dateien des Bootloaders
-/= command Befehl 
-/etc = Host spezifische Systemkonfiguration 
-/usr sekundäre Dateien 
-/var = variable Daten 
-/opt  zusätzliche Anwendungsprogramme
-/mnt = Dient für temporär eingehängtes Dateinsytem 
-/dev=  Gerätedateien 

 Git
- git config
- git init
- git clone
- git add
- git commit
- git diff
- git reset
- git status
- git rm
- git log
- git show
- git tag
- git branch
- git checkout
- git merge
- git remote
- git push
- git pull
- git stas

Man Man man man - eine Oberfläche für die Online Referenzhandbücher

-cd: Durch cd springt man zum /home Verzeichnis

-Is: Listet Dateien und Ordner im aktuellen Verzeichnis
-touch: Erstellt Dateien
-Mkdi:  Erstellt Ordner
-Cd ... Durch Cd .. springt man zum genannten Verzeichnis 
-mv: Durch mv ..  bewegt man dateien ins genannte Verzeichnis

-cp: Kopiert alles mögliche

-rm: Löscht alles mögliche 

-ps: Status von Prozessen 

-top: Zeigt in Echtzeit die aktuellen Prozesse an

-pwd: Zeigten den aktuellen Verzeichnis an

-clear: Löscht den Verlauf

-cat: Gibt den inhalt der Datei an

-df: Zeigt denn Speicher in dem Datensystems an

-du: Zeigt denn Festplatten speicher an

-whoami: Zeigt an in welchen User man ist

-passwd: Ändert das Passwort

-date: Zeigt denn Datum + Uhrzeit an

-Diff:  Zeigt unterschied zwischen Dateien an

-find: Zeigt alle Ordner und und deren Datein 

-grep: Dient zur Suche und Filterung definierter Zeichenketten aus Dateien 

-Isof: Auflistung aller Arten von Linux-Dateien

-rename: Dient um namen von Dateien und Verzeichnisse zu ändern

-Sync:  Dient zur Zwischenspeicherung von änderungen

-Less:  Dient zur anzeigen von Dateien

-Stat: Zeigt die Infos über die Dateien und den Dateinensystem

-Uptime:Zeigt wie lange der Server online ist
